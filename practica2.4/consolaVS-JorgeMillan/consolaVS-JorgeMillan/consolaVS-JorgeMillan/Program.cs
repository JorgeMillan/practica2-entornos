﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consolaVS_JorgeMillan
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion, saldo = 0;
            String opcionpedir;
            do
            {
                System.Console.WriteLine("------MENU------");
                System.Console.WriteLine("1.Meter saldo");
                System.Console.WriteLine("2.Sacar saldo");
                System.Console.WriteLine("3.Ver saldo");
                System.Console.WriteLine("4.Salir");
                do
                {
                    System.Console.WriteLine("\nElige opción: ");
                    opcionpedir =System.Console.ReadLine();
                    opcion = int.Parse(opcionpedir);
                    if (opcion < 0 || opcion > 4) System.Console.WriteLine("Elige una opcion del 1 al 4");
                } while (opcion < 0 || opcion > 4);
                switch (opcion)
                {
                    case 1: saldo = meter(saldo); break;
                    case 2: saldo = sacar(saldo); break;
                    case 3: saldo = ver(saldo); break;
                    case 4: salir(); break;
                }
            } while (opcion < 4);
        }
        private static void salir()
        {
            Environment.Exit(0);
        }
        private static int ver(int saldo)
        {
            System.Console.WriteLine("Ahora mismo hay en cuenta " + saldo + " euros");
            return saldo;
        }
        private static int sacar(int saldo)
        {
            int sacar;
            String pedirsacar;
            do
            {
                System.Console.WriteLine("\tCantidad a sacar:");
                pedirsacar =System.Console.ReadLine();
                sacar = int.Parse(pedirsacar);
                saldo = saldo - sacar;
                if (sacar < 0) System.Console.WriteLine("Introduce una cantidad positiva");
            } while (sacar < 0);
            return saldo;
        }
        private static int meter(int saldo)
        {
            int meter;
            String pedirmeter;
            do
            {
                System.Console.WriteLine("\tCantidad a meter:");
                pedirmeter =System.Console.ReadLine();
                meter = int.Parse(pedirmeter);
                saldo = saldo + meter;
                if (meter < 0) System.Console.WriteLine("Introduce una cantidad positiva");
            } while (meter < 0);
            return saldo;
        }
    }
}

