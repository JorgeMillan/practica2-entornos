package main;
import java.util.Scanner;
public class Main {
static Scanner in=new Scanner(System.in);
	public static void main(String[] args) {
		int opcion, saldo=0;

		do{
			System.out.println("------MENU------");
			System.out.println("1.Meter saldo");
			System.out.println("2.Sacar saldo");
			System.out.println("3.Ver saldo");
			System.out.println("4.Salir");
			do{
				System.out.print("\nElige opci�n: ");
				opcion=in.nextInt();
				if(opcion<0 || opcion>4)System.out.println("Elige una opcion del 1 al 4");
			}while(opcion<0 || opcion>4);
			switch (opcion){
			case 1: saldo=meter(saldo);break;
			case 2: saldo=sacar(saldo);break;
			case 3: saldo=ver(saldo);break;
			case 4: salir();break;
			}
		}while (opcion<4);
	}
	private static void salir() {
		System.exit(0);
	}
	private static int ver(int saldo) {
		System.out.println("Ahora mismo hay en cuenta "+saldo+" euros");
		return saldo;
	}
	private static int sacar(int saldo) {
		int sacar;
		do{
			System.out.println("\tCantidad a sacar:");
			sacar=in.nextInt();
			saldo=saldo-sacar;
			if (sacar<0)System.out.println("Introduce una cantidad positiva");
		}while(sacar<0);
		return saldo;
	}
	private static int meter(int saldo) {
		int meter;
		do{
			System.out.println("\tCantidad a meter:");
			meter=in.nextInt();
			saldo=saldo+meter;
			if (meter<0)System.out.println("Introduce una cantidad positiva");
		}while(meter<0);
		return saldo;
	}
}
