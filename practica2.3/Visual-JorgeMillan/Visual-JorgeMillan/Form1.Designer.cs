﻿namespace Visual_JorgeMillan
{
    /// <summary>
    /// AUTOR Jorge Millán
    /// 26/01/2018 11:44:05AM
    /// </summary>
    partial class Form1
    {
        /// <summary>
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.javaProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projectToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.packageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.interfaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annotationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sourceFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.javaWorkingSetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.untitledTextFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jUnitTextCaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xtendClassToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exampleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.revertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rEnameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convertLineDelimitersToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowsCRLFrn0D0AdefaultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unixLFn0AToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.switchWorkspaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.restartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.propiertisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sourceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.navigateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openProyectsFromFileSystemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.domainUpDown1 = new System.Windows.Forms.DomainUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.sourceToolStripMenuItem,
            this.reToolStripMenuItem,
            this.navigateToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.projectToolStripMenuItem,
            this.runToolStripMenuItem,
            this.windowToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(460, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openFileToolStripMenuItem,
            this.openProyectsFromFileSystemToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.closeAllToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.saveAllToolStripMenuItem,
            this.revertToolStripMenuItem,
            this.moveToolStripMenuItem,
            this.rEnameToolStripMenuItem,
            this.refreshToolStripMenuItem,
            this.convertLineDelimitersToToolStripMenuItem,
            this.printToolStripMenuItem,
            this.switchWorkspaceToolStripMenuItem,
            this.restartToolStripMenuItem,
            this.importToolStripMenuItem,
            this.exportToolStripMenuItem,
            this.propiertisToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.javaProjectToolStripMenuItem,
            this.projectToolStripMenuItem1,
            this.packageToolStripMenuItem,
            this.classToolStripMenuItem,
            this.interfaceToolStripMenuItem,
            this.enumToolStripMenuItem,
            this.annotationToolStripMenuItem,
            this.sourceFolderToolStripMenuItem,
            this.javaWorkingSetToolStripMenuItem,
            this.folderToolStripMenuItem,
            this.fileToolStripMenuItem1,
            this.untitledTextFileToolStripMenuItem,
            this.jUnitTextCaseToolStripMenuItem,
            this.xtendClassToolStripMenuItem,
            this.exampleToolStripMenuItem,
            this.otherToolStripMenuItem});
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.newToolStripMenuItem.Text = "New";
            // 
            // javaProjectToolStripMenuItem
            // 
            this.javaProjectToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("javaProjectToolStripMenuItem.Image")));
            this.javaProjectToolStripMenuItem.Name = "javaProjectToolStripMenuItem";
            this.javaProjectToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.javaProjectToolStripMenuItem.Text = "Java Project";
            // 
            // projectToolStripMenuItem1
            // 
            this.projectToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("projectToolStripMenuItem1.Image")));
            this.projectToolStripMenuItem1.Name = "projectToolStripMenuItem1";
            this.projectToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.projectToolStripMenuItem1.Text = "Project...";
            // 
            // packageToolStripMenuItem
            // 
            this.packageToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("packageToolStripMenuItem.Image")));
            this.packageToolStripMenuItem.Name = "packageToolStripMenuItem";
            this.packageToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.packageToolStripMenuItem.Text = "Package";
            // 
            // classToolStripMenuItem
            // 
            this.classToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("classToolStripMenuItem.Image")));
            this.classToolStripMenuItem.Name = "classToolStripMenuItem";
            this.classToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.classToolStripMenuItem.Text = "Class";
            // 
            // interfaceToolStripMenuItem
            // 
            this.interfaceToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("interfaceToolStripMenuItem.Image")));
            this.interfaceToolStripMenuItem.Name = "interfaceToolStripMenuItem";
            this.interfaceToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.interfaceToolStripMenuItem.Text = "Interface";
            // 
            // enumToolStripMenuItem
            // 
            this.enumToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("enumToolStripMenuItem.Image")));
            this.enumToolStripMenuItem.Name = "enumToolStripMenuItem";
            this.enumToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.enumToolStripMenuItem.Text = "Enum";
            // 
            // annotationToolStripMenuItem
            // 
            this.annotationToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("annotationToolStripMenuItem.Image")));
            this.annotationToolStripMenuItem.Name = "annotationToolStripMenuItem";
            this.annotationToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.annotationToolStripMenuItem.Text = "Annotation";
            // 
            // sourceFolderToolStripMenuItem
            // 
            this.sourceFolderToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("sourceFolderToolStripMenuItem.Image")));
            this.sourceFolderToolStripMenuItem.Name = "sourceFolderToolStripMenuItem";
            this.sourceFolderToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.sourceFolderToolStripMenuItem.Text = "Source Folder";
            // 
            // javaWorkingSetToolStripMenuItem
            // 
            this.javaWorkingSetToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("javaWorkingSetToolStripMenuItem.Image")));
            this.javaWorkingSetToolStripMenuItem.Name = "javaWorkingSetToolStripMenuItem";
            this.javaWorkingSetToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.javaWorkingSetToolStripMenuItem.Text = "Java Working Set";
            // 
            // folderToolStripMenuItem
            // 
            this.folderToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("folderToolStripMenuItem.Image")));
            this.folderToolStripMenuItem.Name = "folderToolStripMenuItem";
            this.folderToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.folderToolStripMenuItem.Text = "Folder";
            // 
            // fileToolStripMenuItem1
            // 
            this.fileToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("fileToolStripMenuItem1.Image")));
            this.fileToolStripMenuItem1.Name = "fileToolStripMenuItem1";
            this.fileToolStripMenuItem1.Size = new System.Drawing.Size(163, 22);
            this.fileToolStripMenuItem1.Text = "File";
            // 
            // untitledTextFileToolStripMenuItem
            // 
            this.untitledTextFileToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("untitledTextFileToolStripMenuItem.Image")));
            this.untitledTextFileToolStripMenuItem.Name = "untitledTextFileToolStripMenuItem";
            this.untitledTextFileToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.untitledTextFileToolStripMenuItem.Text = "Untitled Text File";
            // 
            // jUnitTextCaseToolStripMenuItem
            // 
            this.jUnitTextCaseToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("jUnitTextCaseToolStripMenuItem.Image")));
            this.jUnitTextCaseToolStripMenuItem.Name = "jUnitTextCaseToolStripMenuItem";
            this.jUnitTextCaseToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.jUnitTextCaseToolStripMenuItem.Text = "JUnit Text Case";
            // 
            // xtendClassToolStripMenuItem
            // 
            this.xtendClassToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("xtendClassToolStripMenuItem.Image")));
            this.xtendClassToolStripMenuItem.Name = "xtendClassToolStripMenuItem";
            this.xtendClassToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.xtendClassToolStripMenuItem.Text = "Task";
            // 
            // exampleToolStripMenuItem
            // 
            this.exampleToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exampleToolStripMenuItem.Image")));
            this.exampleToolStripMenuItem.Name = "exampleToolStripMenuItem";
            this.exampleToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.exampleToolStripMenuItem.Text = "Example...";
            // 
            // otherToolStripMenuItem
            // 
            this.otherToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("otherToolStripMenuItem.Image")));
            this.otherToolStripMenuItem.Name = "otherToolStripMenuItem";
            this.otherToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.otherToolStripMenuItem.Text = "Other...";
            // 
            // openFileToolStripMenuItem
            // 
            this.openFileToolStripMenuItem.Name = "openFileToolStripMenuItem";
            this.openFileToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.openFileToolStripMenuItem.Text = "Open File...";
            this.openFileToolStripMenuItem.Click += new System.EventHandler(this.openFileToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.closeToolStripMenuItem.Text = "Close";
            // 
            // closeAllToolStripMenuItem
            // 
            this.closeAllToolStripMenuItem.Name = "closeAllToolStripMenuItem";
            this.closeAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.W)));
            this.closeAllToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.closeAllToolStripMenuItem.Text = "Close All";
            this.closeAllToolStripMenuItem.Click += new System.EventHandler(this.closeAllToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveAsToolStripMenuItem.Image")));
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.saveAsToolStripMenuItem.Text = "Save As..";
            // 
            // saveAllToolStripMenuItem
            // 
            this.saveAllToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveAllToolStripMenuItem.Image")));
            this.saveAllToolStripMenuItem.Name = "saveAllToolStripMenuItem";
            this.saveAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveAllToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.saveAllToolStripMenuItem.Text = "Save All";
            // 
            // revertToolStripMenuItem
            // 
            this.revertToolStripMenuItem.Name = "revertToolStripMenuItem";
            this.revertToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.revertToolStripMenuItem.Text = "Revert";
            // 
            // moveToolStripMenuItem
            // 
            this.moveToolStripMenuItem.Name = "moveToolStripMenuItem";
            this.moveToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.moveToolStripMenuItem.Text = "Move...";
            // 
            // rEnameToolStripMenuItem
            // 
            this.rEnameToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("rEnameToolStripMenuItem.Image")));
            this.rEnameToolStripMenuItem.Name = "rEnameToolStripMenuItem";
            this.rEnameToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.rEnameToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.rEnameToolStripMenuItem.Text = "Rename...";
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.refreshToolStripMenuItem.Text = "Refresh";
            // 
            // convertLineDelimitersToToolStripMenuItem
            // 
            this.convertLineDelimitersToToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.windowsCRLFrn0D0AdefaultToolStripMenuItem,
            this.unixLFn0AToolStripMenuItem});
            this.convertLineDelimitersToToolStripMenuItem.Name = "convertLineDelimitersToToolStripMenuItem";
            this.convertLineDelimitersToToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.convertLineDelimitersToToolStripMenuItem.Text = "Convert Line Delimiters To";
            // 
            // windowsCRLFrn0D0AdefaultToolStripMenuItem
            // 
            this.windowsCRLFrn0D0AdefaultToolStripMenuItem.Name = "windowsCRLFrn0D0AdefaultToolStripMenuItem";
            this.windowsCRLFrn0D0AdefaultToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.windowsCRLFrn0D0AdefaultToolStripMenuItem.Text = "Windows (CRLF, \\r\\n, 0D0A) [default]";
            // 
            // unixLFn0AToolStripMenuItem
            // 
            this.unixLFn0AToolStripMenuItem.Name = "unixLFn0AToolStripMenuItem";
            this.unixLFn0AToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.unixLFn0AToolStripMenuItem.Text = "Unix (LF, \\n 0A)";
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.printToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.printToolStripMenuItem.Text = "Print...";
            // 
            // switchWorkspaceToolStripMenuItem
            // 
            this.switchWorkspaceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.otherToolStripMenuItem1});
            this.switchWorkspaceToolStripMenuItem.Name = "switchWorkspaceToolStripMenuItem";
            this.switchWorkspaceToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.switchWorkspaceToolStripMenuItem.Text = "Switch Workspace";
            // 
            // otherToolStripMenuItem1
            // 
            this.otherToolStripMenuItem1.Name = "otherToolStripMenuItem1";
            this.otherToolStripMenuItem1.Size = new System.Drawing.Size(113, 22);
            this.otherToolStripMenuItem1.Text = "Other...";
            // 
            // restartToolStripMenuItem
            // 
            this.restartToolStripMenuItem.Name = "restartToolStripMenuItem";
            this.restartToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.restartToolStripMenuItem.Text = "Restart";
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("importToolStripMenuItem.Image")));
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.importToolStripMenuItem.Text = "Import...";
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exportToolStripMenuItem.Image")));
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.exportToolStripMenuItem.Text = "Export...";
            // 
            // propiertisToolStripMenuItem
            // 
            this.propiertisToolStripMenuItem.Name = "propiertisToolStripMenuItem";
            this.propiertisToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Return)));
            this.propiertisToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.propiertisToolStripMenuItem.Text = "Properties";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Enabled = false;
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // sourceToolStripMenuItem
            // 
            this.sourceToolStripMenuItem.Enabled = false;
            this.sourceToolStripMenuItem.Name = "sourceToolStripMenuItem";
            this.sourceToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.sourceToolStripMenuItem.Text = "Source";
            // 
            // reToolStripMenuItem
            // 
            this.reToolStripMenuItem.Enabled = false;
            this.reToolStripMenuItem.Name = "reToolStripMenuItem";
            this.reToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.reToolStripMenuItem.Text = "Refactor";
            // 
            // navigateToolStripMenuItem
            // 
            this.navigateToolStripMenuItem.Enabled = false;
            this.navigateToolStripMenuItem.Name = "navigateToolStripMenuItem";
            this.navigateToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.navigateToolStripMenuItem.Text = "Navigate";
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Enabled = false;
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.searchToolStripMenuItem.Text = "Search";
            // 
            // projectToolStripMenuItem
            // 
            this.projectToolStripMenuItem.Enabled = false;
            this.projectToolStripMenuItem.Name = "projectToolStripMenuItem";
            this.projectToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.projectToolStripMenuItem.Text = "Project";
            // 
            // runToolStripMenuItem
            // 
            this.runToolStripMenuItem.Enabled = false;
            this.runToolStripMenuItem.Name = "runToolStripMenuItem";
            this.runToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.runToolStripMenuItem.Text = "Run";
            // 
            // windowToolStripMenuItem
            // 
            this.windowToolStripMenuItem.Enabled = false;
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            this.windowToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.windowToolStripMenuItem.Text = "Window";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Enabled = false;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // openProyectsFromFileSystemToolStripMenuItem
            // 
            this.openProyectsFromFileSystemToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openProyectsFromFileSystemToolStripMenuItem.Image")));
            this.openProyectsFromFileSystemToolStripMenuItem.Name = "openProyectsFromFileSystemToolStripMenuItem";
            this.openProyectsFromFileSystemToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.openProyectsFromFileSystemToolStripMenuItem.Text = "Open Proyects From File System";
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(12, 390);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 27);
            this.button1.TabIndex = 1;
            this.button1.Text = "Cancelar";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(338, 390);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 27);
            this.button2.TabIndex = 2;
            this.button2.Text = "Aceptar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(31, 52);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(76, 70);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 27F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(106, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(302, 33);
            this.label1.TabIndex = 4;
            this.label1.Text = "Encuesta de satisfacción";
            // 
            // domainUpDown1
            // 
            this.domainUpDown1.Items.Add("1");
            this.domainUpDown1.Items.Add("2");
            this.domainUpDown1.Items.Add("3");
            this.domainUpDown1.Items.Add("4");
            this.domainUpDown1.Items.Add("5");
            this.domainUpDown1.Items.Add("6");
            this.domainUpDown1.Items.Add("7");
            this.domainUpDown1.Items.Add("8");
            this.domainUpDown1.Items.Add("9");
            this.domainUpDown1.Items.Add("10");
            this.domainUpDown1.Location = new System.Drawing.Point(414, 152);
            this.domainUpDown1.Name = "domainUpDown1";
            this.domainUpDown1.Size = new System.Drawing.Size(34, 20);
            this.domainUpDown1.TabIndex = 6;
            this.domainUpDown1.Text = "10";
            this.domainUpDown1.SelectedItemChanged += new System.EventHandler(this.domainUpDown1_SelectedItemChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(227, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(181, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Grado de satisfacción con la interfaz";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(159, 189);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Grado de satisfacción general";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(12, 250);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(436, 45);
            this.trackBar1.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 222);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Muy mal";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(398, 222);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Muy bien";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.textBox1.Font = new System.Drawing.Font("Arial Narrow", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(14, 290);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(433, 86);
            this.textBox1.TabIndex = 12;
            this.textBox1.Text = "Comentarios...";
            // 
            // Form1
            // 
            this.AcceptButton = this.button2;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.CancelButton = this.button1;
            this.ClientSize = new System.Drawing.Size(460, 429);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.domainUpDown1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Eklipse";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sourceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem navigateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem javaProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projectToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem packageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem interfaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enumToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annotationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sourceFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem javaWorkingSetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem folderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem untitledTextFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jUnitTextCaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xtendClassToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exampleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem revertToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rEnameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convertLineDelimitersToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem switchWorkspaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restartToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem propiertisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowsCRLFrn0D0AdefaultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unixLFn0AToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem openProyectsFromFileSystemToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DomainUpDown domainUpDown1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox1;
    }
}

