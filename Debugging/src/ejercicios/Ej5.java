package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qu� realiza este programa
		 * 
		 * 
		 * Lo que hace este programa es sacar si un n�mero es primo
		 * o no dividi�ndolo entre sus n�meros anteriores; 
		 * si la cantidad de divisores es dos o menos, el n�mero
		 * ser� primo.
		 * 
		 * Un n�mero es primo si tiene EXACTAMENTE dos divisores
		 * por lo que el programa est� mal cuando metes el 1, 
		 * que no es primo pero da como tal. Correcci�n m�s abajo
		 * 
		 */
		
		
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		//Correccion:
		if(cantidadDivisores >/*cambiar  ">"  por  "!="  */ 2 ){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
