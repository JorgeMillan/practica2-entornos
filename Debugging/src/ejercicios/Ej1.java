package ejercicios;

import java.util.Scanner;

public class Ej1 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 *  
		 */
		
		
		
		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;
		
		cantidadVocales = 0;
		cantidadCifras = 0;
		
		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();
		/*
		 * El fallo esta en la i, deberia ser el numero de letras-1
		 * porque as� siempre esta pidiendo una letra m�s de las las que hemos introducido
		 * y da el error outOfBounds propio del charAT
		 * se arreglaria poniendo en el for:
		 * 
		 * for(int i = 0 ; i <= (cadenaLeida.length()-1); i++){
		 * 
		 */
		for(int i = 0 ; i <= (cadenaLeida.length()-1); i++){
			caracter = cadenaLeida.charAt(i);

			if(caracter == 'a' || caracter == 'e' || caracter == 'i' 
					|| caracter == 'o' || caracter == 'u'){
					cantidadVocales++;
			}
			if(caracter >= '0' && caracter <='9'){
				cantidadCifras++;
			}
			
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);
		
		
		input.close();
	}

}
