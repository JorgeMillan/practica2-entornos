package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		/*
		 * Hace falta limpiar el teclado para coger una cadena
		 * si antes hemos cogido un n�mero, porque sin� coger�
		 * el intro al meter el numero:
		 */
		input.nextLine();
		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();
		/*
		 * Hace falta limpiar el teclado para coger una cadena
		 * si antes hemos cogido un n�mero, porque sin� coger�
		 * el intro al meter el numero:
		 * 
		 * in.nextLine();
		 */
		
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		}
		
		input.close();
		
		
		
	}
}
