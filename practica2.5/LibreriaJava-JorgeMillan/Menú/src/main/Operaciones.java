package main;
import java.util.Scanner;

public class Operaciones {
	int saldo;
	static int pagar(int saldo){
		int pagado=1000;
			saldo=saldo-pagado;
			if (saldo<0)System.out.println("Se te ha quedado la cuenta en numeros negativos");
			if (saldo>0)System.out.println("Prestamo pagado");
		return saldo;
	}
	static void salir() {
		System.exit(0);
	}
	static int ver(int saldo) {
		System.out.println("Ahora mismo hay en cuenta "+saldo+" euros");
		return saldo;
	}
	static int sacar(int saldo) {
		Scanner in=new Scanner(System.in);
		int sacar;
		do{
			System.out.println("\tCantidad a sacar:");
			sacar=in.nextInt();
			saldo=saldo-sacar;
			if (sacar<0)System.out.println("Introduce una cantidad positiva");
		}while(sacar<0);
		return saldo;
	}
	static int meter(int saldo) {
		Scanner in=new Scanner(System.in);
		int meter;
		do{
			System.out.println("\tCantidad a meter:");
			meter=in.nextInt();
			saldo=saldo+meter;
			if (meter<0)System.out.println("Introduce una cantidad positiva");
		}while(meter<0);
		return saldo;
	}
}
