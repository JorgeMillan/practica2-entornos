package main;
import java.util.Scanner;
public class Main {
static Scanner in=new Scanner(System.in);
	public static void main(String[] args) {
		int opcion, saldo=0;
		do{
			System.out.println("------MENU------");
			System.out.println("1.Meter saldo");
			System.out.println("2.Sacar saldo");
			System.out.println("3.Ver saldo");
			System.out.println("4.Pagar pr�stamo de la casa");
			System.out.println("5.Salir");
			do{
				System.out.print("\nElige opci�n: ");
				opcion=in.nextInt();
				if(opcion<0 || opcion>5)System.out.println("Elige una opcion del 1 al 5");
			}while(opcion<0 || opcion>5);
			switch (opcion){
			case 1: saldo=Operaciones.meter(saldo);break;
			case 2: saldo=Operaciones.sacar(saldo);break;
			case 3: saldo=Operaciones.ver(saldo);break;
			case 4: saldo=Operaciones.pagar(saldo);break;
			case 5: Operaciones.salir();break;
			}
		}while (opcion<5);
	}
}