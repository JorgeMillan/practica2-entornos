﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menú
{
    public class Operaciones
    {
        int saldo;
        public static int pagar(int saldo)
        {
            int pagado = 1000;
            saldo = saldo - pagado;
            if (saldo < 0) Console.WriteLine("Se te ha quedado la cuenta en numeros negativos");
            if (saldo > 0) Console.WriteLine("Prestamo pagado");
            return saldo;
        }
        public static void salir()
        {
            Environment.Exit(0);
        }
        public static int ver(int saldo)
        {
            Console.WriteLine("Ahora mismo hay en cuenta " + saldo + " euros");
            return saldo;
        }
        public static int sacar(int saldo)
        {
            int sacar;
            String sacarpedir;
            do
            {
                Console.WriteLine("\tCantidad a sacar:");
                sacarpedir = System.Console.ReadLine();
                sacar = int.Parse(sacarpedir);
                saldo = saldo - sacar;
                if (sacar < 0) Console.WriteLine("Introduce una cantidad positiva");
            } while (sacar < 0);
            return saldo;
        }
        public static int meter(int saldo)
        {
            int meter;
            String meterpedir;
            do
            {
                Console.WriteLine("\tCantidad a meter:");
                meterpedir = System.Console.ReadLine();
                meter = int.Parse(meterpedir);
                saldo = saldo + meter;
                if (meter < 0) Console.WriteLine("Introduce una cantidad positiva");
            } while (meter < 0);
            return saldo;
        }
    }
}