﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menú
{
    class Program
    {
        static void Main(string[] args)
        {
                int opcion, saldo = 0;
                String opcionpedir;
                do
                {
                    Console.WriteLine("------MENU------");
                    Console.WriteLine("1.Meter saldo");
                    Console.WriteLine("2.Sacar saldo");
                    Console.WriteLine("3.Ver saldo");
                    Console.WriteLine("4.Pagar préstamo de la casa");
                    Console.WriteLine("5.Salir");
                    do
                    {
                        Console.WriteLine("\nElige opción: ");
                        opcionpedir = System.Console.ReadLine();
                        opcion = int.Parse(opcionpedir);
                    if (opcion < 0 || opcion > 5) Console.WriteLine("Elige una opcion del 1 al 5");
                    } while (opcion < 0 || opcion > 5);
                    switch (opcion)
                    {
                        case 1: saldo = Operaciones.meter(saldo); break;
                        case 2: saldo = Operaciones.sacar(saldo); break;
                        case 3: saldo = Operaciones.ver(saldo); break;
                        case 4: saldo = Operaciones.pagar(saldo); break;
                        case 5: Operaciones.salir(); break;
                    }
                } while (opcion < 5);
            }
        }
    }